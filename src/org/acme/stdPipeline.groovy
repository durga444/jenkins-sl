#!/bin/groovy
package org.acme;
import org.yaml.snakeyaml.Yaml
@Grab('org.yaml:snakeyaml:1.23')

def execute() {

  node {

    stage('Initialize') {
      checkout scm
      echo 'Loading pipeline definition'
      Yaml parser = new Yaml()
      Map pipelineDefinition = parser.load(new File(pwd() + '/pipeline.yml').text)
    }

    switch(pipelineDefinition.pipelineType) {
      case 'python':
        // Instantiate and execute a Python pipeline
            pythonPipeline(pipelineDefinition).executePipeline()
      case 'nodejs':
        // Instantiate and execute a NodeJS pipeline
            nodeJSPipeline(pipelineDefinition).executePipeline()
    }

  }

}
